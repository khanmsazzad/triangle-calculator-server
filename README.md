# This is my README
As the name suggests, this is the server side program for the RPC Triangle Calculator.
The server side blindly calculates the perimeter and area without checking null values or such because client side already ensures this.
In the current coding, client never sends value for with angle parameter although server is ready to serve it. 

For coverage report:

You need maven 3. Run mvn clean install inside the project directory, you will find the report in target/site folder. 

