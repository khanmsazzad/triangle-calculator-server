package org.sazzad.triangle.Server;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class MessageCreatorTest {
	@Test
	public void testInstanceCreate() {
		MessageCreator messageCreator = new MessageCreator();
		assertNotNull("InstanceCreation Check", messageCreator);
	}

	@Test
	public void testCreateMessageTest() {
		byte[] bytes = MessageCreator.createMessage();
		assertNotNull("Returned Array Should not be null", bytes);
	}

	@Test
	public void testCreateMessageWithParams() {
		byte[] bytes = MessageCreator.createMessage(30, 30, new float[] { 5,
				12, 13 });
		assertNotNull("Returned Array Should not be null", bytes);
	}
}
