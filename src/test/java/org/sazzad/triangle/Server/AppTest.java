package org.sazzad.triangle.Server;

import java.util.Arrays;

import junit.framework.TestCase;

import org.junit.Test;

public class AppTest extends TestCase {

	@Test
	public void testAppInstanceCreate() {
		App app = new App();
		assertNotNull("App instance create", app);
	}

	@Test
	public void testAppStaticMainMethod() {
		try {
			App.main(new String[] { "true" });
		} catch (Exception e) {
			fail("Should fail if exception thrown");
		}
	}

	@Test
	public void testCalculateArea() {
		assertEquals(ResultCalculator.calculateArea(8, 8, 8), 27.7128, 0.02);
	}

	@Test
	public void testCalculatePerimeter() {
		assertEquals(ResultCalculator.calculatePerimeter(8, 8, 8), 24.0, 0.02);
	}

	@Test
	public void testCalculateAngle() {
		float[] expected = { 60.0F, 60.0F, 60.0F };
		assertTrue(Arrays.equals(expected,
				ResultCalculator.calculateAngle(8, 8, 8)));
	}

}
