package org.sazzad.triangle.Server;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import junit.framework.TestCase;

import org.junit.Test;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

public class CommunicatorTest extends TestCase {
	@Test
	public void testCommunicatorInstanceCreate() {
		Communicator communicator = new Communicator(new ConnectionFactory());
		assertNotNull("Communicator instance cretae check", communicator);
	}

	@Test
	public void testMockCreation() throws IOException {
		ConnectionFactory mockConnectionFactory = mock(ConnectionFactory.class);
		Connection mockConnection = mock(Connection.class);
		Channel mockChannel = mock(Channel.class);
		QueueingConsumer.Delivery mockDelivery = mock(QueueingConsumer.Delivery.class);

		when(mockConnection.createChannel()).thenReturn(mockChannel);
		when(mockConnectionFactory.newConnection()).thenReturn(mockConnection);
		when(mockDelivery.getBody())
				.thenReturn(
						MessageCreator.createMessage(30, 30, new float[] { 5,
								12, 13 }));

		Communicator communicator = new Communicator(mockConnectionFactory,
				true, mockDelivery);
		try {
			communicator.runCommunication();
		} catch (Exception e) {
			fail("Should fail if exception thrown");
		} finally {
			mockConnection.close();
			mockConnectionFactory = null;
		}

		mockConnectionFactory = mock(ConnectionFactory.class);
		mockConnection = mock(Connection.class);
		mockChannel = mock(Channel.class);
		mockDelivery = mock(QueueingConsumer.Delivery.class);

		when(mockConnection.createChannel()).thenReturn(mockChannel);
		when(mockConnectionFactory.newConnection()).thenReturn(mockConnection);
		when(mockDelivery.getBody()).thenReturn(MessageCreator.createMessage());

		communicator = new Communicator(mockConnectionFactory, true,
				mockDelivery);
		try {
			communicator.runCommunication();
		} catch (Exception e) {
			e.printStackTrace();
			fail("Should fail if exception thrown");
		} finally {
			mockConnection.close();
			mockConnectionFactory = null;
		}
	}
}
