package org.sazzad.triangle.Server;

import com.rabbitmq.client.ConnectionFactory;

public class App {
	public static void main(String[] argv) {
		ConnectionFactory connectionFactory = new ConnectionFactory();
		Communicator communicator;
		if (argv != null && argv.length != 0) {
			communicator = new Communicator(connectionFactory,
					Boolean.valueOf(argv[0]));
		} else {
			communicator = new Communicator(connectionFactory);
		}
		communicator.runCommunication();
	}
}