package org.sazzad.triangle.Server;

import org.sazzad.triangle.Server.TriangleProtos.Triangle;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

public class Communicator {

	private static final String RPC_QUEUE_NAME = "rpc_queue";
	private ConnectionFactory factory;
	private boolean isTestCaseRunning = false;
	private QueueingConsumer.Delivery delivery;

	public Communicator(ConnectionFactory connectionFactory) {
		factory = connectionFactory;
	}

	public Communicator(ConnectionFactory connectionFactory,
			boolean isTestCaseRunning) {
		factory = connectionFactory;
		this.isTestCaseRunning = isTestCaseRunning;
	}

	public Communicator(ConnectionFactory connectionFactory,
			boolean isTestCaseRunning, QueueingConsumer.Delivery pDelivery) {
		factory = connectionFactory;
		this.isTestCaseRunning = isTestCaseRunning;
		this.delivery = pDelivery;
	}

	public void runCommunication() {
		Connection connection = null;
		Channel channel = null;
		try {
			factory.setHost("localhost");

			connection = factory.newConnection();
			channel = connection.createChannel();

			channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);

			channel.basicQos(1);

			QueueingConsumer consumer = new QueueingConsumer(channel);
			channel.basicConsume(RPC_QUEUE_NAME, false, consumer);

			System.out.println(" [x] Awaiting RPC requests");

			while (true) {
				byte[] result = null;

				BasicProperties props = null;
				BasicProperties replyProps = null;
				if (!isTestCaseRunning) {
					delivery = consumer.nextDelivery();
					props = delivery.getProperties();
					replyProps = new BasicProperties.Builder().correlationId(
							props.getCorrelationId()).build();
				}

				try {
					Triangle message = Triangle.parseFrom(delivery.getBody());
					String paramType = message.getParameterType().toLowerCase();

					if (paramType.equals("-angle")) {
						result = MessageCreator.createMessage();

					} else if (paramType.equals("-side")) {
						double area = ResultCalculator.calculateArea(
								message.getParamA(), message.getParamB(),
								message.getParamC());
						double perimeter = ResultCalculator.calculatePerimeter(
								message.getParamA(), message.getParamB(),
								message.getParamC());
						float[] angles = ResultCalculator.calculateAngle(
								message.getParamA(), message.getParamB(),
								message.getParamC());
						result = MessageCreator.createMessage(area, perimeter,
								angles);

					}
				} catch (Exception e) {
					System.out.println(e.toString());
					result = "".getBytes();
					throw e;
				} finally {
					if (isTestCaseRunning) {
						break;
					}
					channel.basicPublish("", props.getReplyTo(), replyProps,
							result);
					channel.basicAck(delivery.getEnvelope().getDeliveryTag(),
							false);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (Exception ignore) {
				}
			}
		}
	}
}
