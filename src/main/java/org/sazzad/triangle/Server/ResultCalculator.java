package org.sazzad.triangle.Server;

public class ResultCalculator {
	public static double calculateArea(float paramA, float paramB, float paramC) {
		double s = (double) (calculatePerimeter(paramA, paramB, paramC) * 0.5);
		return (float) Math
				.sqrt(s * (s - paramA) * (s - paramB) * (s - paramC));
	}

	public static double calculatePerimeter(float paramA, float paramB,
			float paramC) {
		return paramA + paramB + paramC;
	}

	public static float[] calculateAngle(float paramA, float paramB,
			float paramC) {
		float[] angles = new float[3];
		float sqrA = (float) Math.pow(paramA, 2);
		float sqrB = (float) Math.pow(paramB, 2);
		float sqrC = (float) Math.pow(paramC, 2);

		angles[0] = (float) (Math.acos((sqrA + sqrB - sqrC)
				/ (2 * paramA * paramB)) * (180 / Math.PI));
		angles[1] = (float) (Math.acos((sqrB + sqrC - sqrA)
				/ (2 * paramB * paramC)) * (180 / Math.PI));
		angles[2] = (float) (Math.acos((sqrC + sqrA - sqrB)
				/ (2 * paramC * paramA)) * (180 / Math.PI));

		return angles;
	}
}
