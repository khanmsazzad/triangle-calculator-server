package org.sazzad.triangle.Server;

import org.sazzad.triangle.Server.TriangleProtos.Triangle;

public class MessageCreator {
	public static byte[] createMessage() {
		Triangle result = Triangle.newBuilder().setParameterType("-angle")
				.build();

		return result.toByteArray();

	}

	public static byte[] createMessage(double area, double perimeter,
			float parameters[]) {
		Triangle result = Triangle.newBuilder().setParameterType("-side")
				.setParamA(parameters[0]).setParamB(parameters[1])
				.setParamC(parameters[2]).setArea(area).setPerimeter(perimeter)
				.build();

		return result.toByteArray();

	}
}
